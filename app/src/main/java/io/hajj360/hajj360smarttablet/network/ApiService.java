package io.hajj360.hajj360smarttablet.network;

import java.util.List;

import io.hajj360.hajj360smarttablet.Group;
import io.hajj360.hajj360smarttablet.User;
import io.reactivex.Single;
import retrofit2.Response;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.POST;

public interface ApiService {

    //Get UserInfo
    @FormUrlEncoded
    @POST("services/find_group")
    Single<List<Group>> getGroupLocation(@Field("service_type") String type,
                                         @Field("user_id") int userId,
                                         @Field("location") String location);

    @FormUrlEncoded
    @POST("users/get_info")
    Single<User> getUserInfo(@Field("code") String code);


    @FormUrlEncoded
    @POST("services/request")
    Single<Response> askForHelp(
            @Field("service_type") String type,
            @Field("user_id") int userId,
            @Field("location") String location
    );

    @FormUrlEncoded
    @POST("users")
    Single<List<User>> getFogLocation(@Field("location_key") String code);}
